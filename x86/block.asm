;*
;* Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
;*
;* This File is part of Advanced Visualization Studio.
;*
;* This program is free software: you can redistribute it and/or modify
;* it under the terms of the GNU General Public License as published by
;* the Free Software Foundation, either version 3 of the License, or
;* (at your option) any later version.
;*
;* This program is distributed in the hope that it will be useful,
;* but WITHOUT ANY WARRANTY; without even the implied warranty of
;* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;* GNU General Public License for more details.
;*
;* You should have received a copy of the GNU General Public License
;* along with this program.  If not, see <http://www.gnu.org/licenses/>.
;*

%include "x86/x86util.asm"
%include "x86/constants.inc"

SECTION_TEXT

%macro emms_if_mmx 0
    %if mmsize == 8
        emms
    %endif
%endmacro

%macro BLOCK 2
cglobal blend_block_%1, 4, 4, 2, 0, output, a, b, pixels
    .loop:
        movu  m0,       [aq]
        movu  m1,       [bq]
        %2    m0,        m1
        movu [outputq],  m0

        add   outputq,   mmsize
        add   aq,        mmsize
        add   bq,        mmsize
        sub   pixelsd,   mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
BLOCK add, paddusb
BLOCK sub, psubusb
BLOCK xor, pxor

INIT_MMX mmx2
BLOCK avg, pavgb
BLOCK max, pmaxub
BLOCK min, pminub

INIT_XMM sse2
BLOCK avg, pavgb
BLOCK add, paddusb
BLOCK sub, psubusb
BLOCK xor, pxor
BLOCK max, pmaxub
BLOCK min, pminub

INIT_YMM avx2
BLOCK avg, pavgb
BLOCK add, paddusb
BLOCK sub, psubusb
BLOCK xor, pxor
BLOCK max, pmaxub
BLOCK min, pminub

%macro BLOCK_MUL 0
cglobal blend_block_mul, 4, 4, 2, 0, output, a, b, pixels
    .loop:
        ; YMM registers seem to lack a half-width move.
        %if mmsize == 32
            movu   xm0,      [aq]
            movu   xm1,      [bq]
        %else
            movh   m0,       [aq]
            movh   m1,       [bq]
        %endif

        punpcklbw  m0,       [pb_0]
        punpcklbw  m1,       [pb_0]
        pmullw     m0,        m1
        psrlw      m0,        8
        packuswb   m0,        m0

        %if mmsize == 32
            movu  [outputq],  xm0
        %else
            movh  [outputq],  m0
        %endif

        add        outputq,   mmsize/2
        add        aq,        mmsize/2
        add        bq,        mmsize/2
        sub        pixelsd,   mmsize/8
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
BLOCK_MUL

INIT_XMM sse2
BLOCK_MUL

INIT_YMM avx2
BLOCK_MUL

%macro BLOCK_ADJ 0
cglobal blend_block_adj, 4, 4, 4, 0, output, a, b, pixels, v
    movd   xm3,  vm
    mova   m2,  [pw_255]
    SPLATW m3,   xm3, 0  ; m3 holds v
    psubw  m2,   m3      ; m2 holds 255-v

    .loop:
        ; YMM registers seem to lack a half-width move.
        %if mmsize == 32
            movu   xm0,      [aq]
            movu   xm1,      [bq]
        %else
            movh   m0,       [aq]
            movh   m1,       [bq]
        %endif

        punpcklbw  m0,       [pb_0]
        punpcklbw  m1,       [pb_0]
        pmullw     m0,        m3
        pmullw     m1,        m2
        paddw      m0,        m1
        psrlw      m0,        8
        packuswb   m0,        m0

        %if mmsize == 32
            movu  [outputq],  xm0
        %else
            movh  [outputq],  m0
        %endif

        add        outputq,   mmsize/2
        add        aq,        mmsize/2
        add        bq,        mmsize/2
        sub        pixelsd,   mmsize/8
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
BLOCK_ADJ

INIT_XMM sse2
BLOCK_ADJ

INIT_YMM avx2
BLOCK_ADJ

%macro BLOCK_CONST 2
cglobal blend_block_const_%1, 4, 4, 2, 0, output, a, const, pixels
    movd   xm1, constd
    SPLATD m1
    .loop:
        movu     m0,       [aq]
        %2       m0,        m1
        movu    [outputq],  m0

        add      outputq,   mmsize
        add      aq,        mmsize
        sub      pixelsd,   mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
BLOCK_CONST add, paddusb
BLOCK_CONST sub1, psubusb
BLOCK_CONST xor, pxor

INIT_MMX mmx2
BLOCK_CONST avg, pavgb
BLOCK_CONST max, pmaxub
BLOCK_CONST min, pminub

INIT_XMM sse2
BLOCK_CONST avg, pavgb
BLOCK_CONST add, paddusb
BLOCK_CONST sub1, psubusb
BLOCK_CONST xor, pxor
BLOCK_CONST max, pmaxub
BLOCK_CONST min, pminub

INIT_YMM avx2
BLOCK_CONST avg, pavgb
BLOCK_CONST add, paddusb
BLOCK_CONST sub1, psubusb
BLOCK_CONST xor, pxor
BLOCK_CONST max, pmaxub
BLOCK_CONST min, pminub

%macro BLOCK_CONST_SUB2 0
cglobal blend_block_const_sub2, 4, 4, 2, mmsize, output, const, a, pixels
    movd    xm1,  constd
    SPLATD  m1
    mova   [rsp], m1
    .loop:
        movu     m0,       [aq]
        mova     m1,       [rsp]
        psubusb  m1,        m0
        movu    [outputq],  m1

        add      outputq,   mmsize
        add      aq,        mmsize
        sub      pixelsd,   mmsize/4
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
BLOCK_CONST_SUB2

INIT_XMM sse2
BLOCK_CONST_SUB2

INIT_YMM avx2
BLOCK_CONST_SUB2

%macro BLOCK_CONST_MUL 0
cglobal blend_block_const_mul, 4, 4, 2, 0, output, a, const, pixels
    movd      xm1,  constd
    SPLATD    m1
    punpcklbw m1,  [pb_0]
    .loop:
        ; YMM registers seem to lack a half-width move.
        %if mmsize == 32
            movu   xm0,      [aq]
        %else
            movh   m0,       [aq]
        %endif

        punpcklbw  m0,       [pb_0]
        pmullw     m0,        m1
        psrlw      m0,        8
        packuswb   m0,        m0

        %if mmsize == 32
            movu  [outputq],  xm0
        %else
            movh  [outputq],  m0
        %endif

        add        outputq,   mmsize/2
        add        aq,        mmsize/2
        sub        pixelsd,   mmsize/8
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
BLOCK_CONST_MUL

INIT_XMM sse2
BLOCK_CONST_MUL

INIT_YMM avx2
BLOCK_CONST_MUL

%macro BLOCK_CONST_ADJ 0
cglobal blend_block_const_adj, 4, 4, 4, 0, output, a, const, pixels, v
    movd      xm1,  constd
    SPLATD    m1
    punpcklbw m1,  [pb_0]

    movd      xm3,  vm
    mova      m2,  [pw_255]
    SPLATW    m3,   xm3, 0  ; m3 holds v
    psubw     m2,   m3      ; m2 holds 255-v

    pmullw    m1,   m2

    .loop:
        ; YMM registers seem to lack a half-width move.
        %if mmsize == 32
            movu   xm0,      [aq]
        %else
            movh   m0,       [aq]
        %endif

        punpcklbw  m0,       [pb_0]
        pmullw     m0,        m3
        paddw      m0,        m1
        psrlw      m0,        8
        packuswb   m0,        m0

        %if mmsize == 32
            movu  [outputq],  xm0
        %else
            movh  [outputq],  m0
        %endif

        add        outputq,   mmsize/2
        add        aq,        mmsize/2
        sub        pixelsd,   mmsize/8
    jg .loop

    emms_if_mmx
RET
%endmacro

INIT_MMX mmx
BLOCK_CONST_ADJ

INIT_XMM sse2
BLOCK_CONST_ADJ

INIT_YMM avx2
BLOCK_CONST_ADJ
