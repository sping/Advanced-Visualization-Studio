/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_AVS
#define AVS_HEADER_AVS 1

#define AVS_API_VERSION 0

#include <stdint.h>

typedef struct AVSContext AVSContext;

/**
 * The structure holding all the input data and the render dimensions.
 */
typedef struct AVSDataContext {
    /**
     * The waveform and spectrum data.
     */
    struct three_channel {
        uint8_t *left;
        uint8_t *right;
        uint8_t *center;
    } waveform, spectrum;

    int width;
    int height;
} AVSDataContext;

/**
 * Open the preset file and initialise the rendering chain.
 *
 * @param actx      address in which to store the opaque context structure.  If
 *                  this is not NULL then some memory has been allocated which
 *                  needs to be freed by avs_uninit().
 * @param filename  the file name of preset to be opened (in UTF-8).
 * @param cpu_flags libavutil compatible CPU feature flags.
 *
 * @return 0 on success.
 */
int avs_init(AVSContext **actx, const char *filename, int cpu_flags);

/**
 * Prints a loaded preset.
 *
 * @param actx pointer to the opaque structure.
 */
void avs_print_preset(AVSContext *actx);

/**
 * Render one (1) frame
 *
 * @param actx  pointer to the opaque structure allocated by avs_init().
 * @param avsdc pointer to the structure holding the input data.
 * @param time  the time of the frame, in milliseconds, used for beat detection.
 *
 * @return 0 on success or a negative number on failure.
 */
int avs_render_frame(AVSContext *actx, AVSDataContext *avsdc, uint32_t time);

/**
 * Get the address of the top-left pixel.
 *
 * @param actx pointer to the opaque structure allocated by avs_init().
 *
 * @return the address of the top-left pixel.  All other pixels of the frame
 *         follow consecutively.
 */
void *avs_get_frame(AVSContext *actx);

/**
 * Frees all memory allocated by AVS.
 *
 * @param actx pointer to the opaque structure allocated by avs_init().  It is
 *             safe to pass a NULL pointer into this function.
 */
void avs_uninit(AVSContext *actx);

#endif
