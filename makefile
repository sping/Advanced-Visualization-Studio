# When make is run without specifying a target on the command line, make will
# default to the first target written in the makefile.
all:

# Disable removal of intermediate files.
.SECONDARY:

# Disable builtin recipes.
.SUFFIXES:

include config.mak
include recipes.mak
include tests/makefile

# Try some quiet rules
ifndef V
Q      = @
ECHO   = printf "$(1)\t%s\n" $(2)
BRIEF  = AR CC LD STRIP YASM
SILENT = CP DEPYASM RANLIB RM

MSG    = $@
M      = @$(call ECHO,$(TAG),$@);
$(foreach VAR,$(BRIEF), $(eval override $(VAR) = @$$(call ECHO,$(VAR),$$(MSG)); $($(VAR))))
$(foreach VAR,$(SILENT),$(eval override $(VAR) = @$($(VAR))))
endif

# List of object files.

LIB_OBJECTS = bpm.o \
              block.o \
              components.o \
              effect_list.o \
              expressions.o \
              linedraw.o \
              main.o \
              matrix.o \
              not_implemented.o \
              pixel.o \
              utils.o \

MISC_OBJECTS = misc/buffer_save.o \
               misc/comment.o \
               misc/render_mode.o \

RENDER_OBJECTS = render/beat_clear.o \
                 render/bass_spin.o \
                 render/clear_screen.o \
                 render/dot_fountain.o \
                 render/dot_grid.o \
                 render/dot_plane.o \
                 render/osc_star.o \
                 render/particle.o \
                 render/ring.o \
                 render/rot_star.o \
                 render/simple.o \
                 render/superscope.o \
                 render/star_field.o \
                 render/timescope.o \

TRANS_OBJECTS = trans/blit.o \
                trans/blur.o \
                trans/brightness.o \
                trans/bump.o \
                trans/channel_shift.o \
                trans/colour_clip.o \
                trans/colour_fade.o \
                trans/colour_map.o \
                trans/colour_modifier.o \
                trans/colour_reduction.o \
                trans/dynamic_distance_modifier.o \
                trans/dynamic_movement.o \
                trans/dynamic_shift.o \
                trans/fadeout.o \
                trans/fast_brightness.o \
                trans/grain.o \
                trans/interferences.o \
                trans/interleave.o \
                trans/invert.o \
                trans/mirror.o \
                trans/mosaic.o \
                trans/movement.o \
                trans/multiplier.o \
                trans/roto_blit.o \
                trans/scatter.o \
                trans/texer.o \
                trans/unique_tone.o \
                trans/video_delay.o \
                trans/water.o \
                trans/water_bump.o \

X86_OBJECTS = x86/block.o \
              x86/channel_shift.o \
              x86/colour_reduction.o \
              x86/constants.o \
              x86/fast_brightness.o \
              x86/init.o \
              x86/multiplier.o \
              x86/unique_tone.o \
              x86/water.o \

LIB_OBJECTS += $(MISC_OBJECTS) $(RENDER_OBJECTS) $(TRANS_OBJECTS)

LIBRARY = libavs.a

PROGRAMS = tests/benchmark \
           tests/test \

X86_PROGRAMS = tests/asm_block \

ifeq ($(ARCH_X86),1)
    LIB_OBJECTS += $(X86_OBJECTS)
    PROGRAMS += $(X86_PROGRAMS)
endif

PROGRAMS_G := $(PROGRAMS:%=%_g$(EXESUF))
PROGRAMS := $(PROGRAMS:%=%$(EXESUF))

ALL_PROGRAMS += $(PROGRAMS_G) $(PROGRAMS)
ALL_OBJECTS += $(LIB_OBJECTS) $(PROGRAMS:%$(EXESUF)=%.o)

# Targets

# Add the true dependencies for the all target.
all: lib cli

cli: $(ALL_PROGRAMS)

lib: $(LIBRARY)

$(LIBRARY): $(LIB_OBJECTS)

$(PROGRAMS_G): $(LIBRARY)

$(X86_OBJECTS): x86/constants.inc

# Recipes

# The use of the 'Q' command in sed here is a GNU extension.
lua-helper.h: doc/function-helper.lua doc/global-helper.lua doc/sanitize-helper.lua
	$(RM) $@
	@printf CREATE\\t$@\\n
	@echo 'static const char *lua_function_helper =' >> $@
	@sed '/^--\[/Q;s|^\(.\+\)$$|"\1\\n"|' doc/function-helper.lua >> $@
	@echo ';' >> $@
	@echo 'static const char *lua_global_helper =' >> $@
	@sed '/^--\[/Q;s|^\(.\+\)$$|"\1\\n"|' doc/global-helper.lua >> $@
	@echo ';' >> $@
	@echo 'static const char *lua_sanitize_helper =' >> $@
	@sed '/^--\[/Q;s|^\(.\+\)$$|"\1\\n"|' doc/sanitize-helper.lua >> $@
	@echo ';' >> $@

x86/constants.inc: x86/constants.asm
	@printf CREATE\\t$@\\n
	@sed '/^const/!d;s|^const|cextern|' $< > $@

expressions.o: lua-helper.h

winamp: vis_jdavs.o lib
	$(LD) $(LDFLAGS) -mwindows -municode -mdll -o $(<:.o=.dll) $< $(LIBRARY) $(LDLIBS) $(LIBS) -lgdi32 -lcomdlg32
	$(CP) $(<:.o=.dll) /cygdrive/c/Winamp/Plugins/

# Use the built-in variable for rm
clean: clean-prof
	$(RM) $(ALL_OBJECTS:.o=.d)
	$(RM) $(ALL_OBJECTS)
	$(RM) $(ALL_PROGRAMS)
	$(RM) $(LIBRARY) lua-helper.h x86/constants.inc

clean-prof:
	$(RM) $(ALL_OBJECTS:.o=.gcda)
	$(RM) $(ALL_OBJECTS:.o=.gcda.info)
	$(RM) $(ALL_OBJECTS:.o=.gcno)

.PHONY: all clean clean-prof cli lib winamp

-include $(ALL_OBJECTS:.o=.d)
