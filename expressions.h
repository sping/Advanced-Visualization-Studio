/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_EXPRESSIONS
#define AVS_HEADER_EXPRESSIONS 1

#include <stdint.h>

#if CONFIG_LUAJIT
#include <luajit-2.0/lua.h>
#include <luajit-2.0/lualib.h>
#include <luajit-2.0/lauxlib.h>
#else
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
#endif

#include "avs.h"

/**
 * Open and initialise an expression context.
 *
 * @param actx pointer to the master AVSContext structure that owns all
 *             components and stores data needed by expressions.
 *
 * @return a pointer to the opaque expression context.
 */
void *expr_init(AVSContext *actx);

/**
 * Close an expression context.
 *
 * @param expr_ctx pointer to the opaque expression context.
 */
void expr_uninit(void *expr_ctx);

/**
 * Read and sanitize an expression string (RString) from a preset.
 *
 * @param expr_ctx pointer to the opaque expression context.
 * @param expr     address in which to store the location of the memory
 *                 allocated for the expression.
 * @param buf      pointer to the start of the expression string (RString).
 * @param buf_len  number of bytes remaining in the buffer.
 *
 * @return a negative number on failure, otherwise the bumber of bytes read.
 */
int expr_load_rstring(void *expr_ctx, const char **expr, const uint8_t *buf,
                      int buf_len);

/**
 * Sanitize an expression string.
 *
 * @param expr_ctx pointer to the opaque expression context.
 * @param expr     a zero-terminated string.
 *
 * @return zero on failure, otherwise the address of a newly allocated,
 *         zero-terminated string.
 */
const char *expr_sanitize(void *expr_ctx, const char *expr);

/**
 * Set a variable in an expression context.
 *
 * @param expr_ctx pointer to the opaque expression context.
 * @param name     the name of the variable to set.
 * @param value    the value to set.
 */
void expr_variable_set(void *expr_ctx, const char *name, double value);

/**
 * Get the value of a variable from an expression context.
 *
 * @param expr_ctx pointer to the opaque expression context.
 * @param name     name of the variable to get.
 *
 * @return the value of the variable.
 */
double expr_variable_get(void *expr_ctx, const char *name);

/**
 * Compile an expression string.
 *
 * @param expr_ctx pointer to the opaque expression context.
 * @param name     the name to give this function.
 * @param expr     the string to compile.
 *
 * @return zero on success.
 */
int expr_compile(void *expr_ctx, const char *name, const char *expr);

/**
 * Prepend arguments and append return values to an expression and then compile
 * the resulting string.  This is intended to be used for an expression in a
 * tight inner loop, such as per point or per pixel.
 *
 * @param expr_ctx pointer to the opaque expression context.
 * @param name     the name to give this function.
 * @param args     the string to be prepended.  Typically the arguments and
 *                 local variables.  Can be 0.
 * @param expr     the expression string.  The body of the function.
 * @param returns  the values to return from the function.  These will be left
 *                 in the expression context after the function has been
 *                 executed with expr_execute2. Can be 0.
 *
 * @return zero on success.
 */
int expr_compile2(void *expr_ctx, const char *name, const char *args,
                  const char *expr, const char *returns);

/**
 * Execute an expression.  No values are left in the expression context.
 *
 * @param expr_ctx pointer to the opaque expression context.
 * @param name     name of the expression to be executed.
 * @param expr     pointer to the expression string, which can be NULL, in that
 *                 case no work is done.
 *
 * @return zero on success.
 */
int expr_execute(void *expr_ctx, const char *name, const char *);

/**
 * Execute an expression with a variable number of arguments.
 *
 * @param expr_ctx pointer to the opaque expression context.
 * @param name     name of the function to execute.
 * @param num_args number of optional arguments.
 * @param ...      some arguments of type double to be passed to the expression
 *                 context function.
 *
 * @return zero on success.
 */
int expr_execute2(void *expr_ctx, const char *name, int num_args, ...);

/**
 * Clear return values from an expression context.
 *
 * @param expr_ctx pointer to the opaque expression context.
 */
void expr_clear_return(void *expr_ctx);

/**
 * Get a return value from an expression context.
 *
 * @param expr_ctx pointer to the opaque expression context.
 * @param which    which value to get, ranging from 1 to the number of return
 *                 values set at expression compile time.
 *
 * @return a double type value.
 */
#define expr_get_return(expr_ctx, which) lua_tonumber(expr_ctx, which)
#endif
