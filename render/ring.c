/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "linedraw.h"

enum old_effect {
    O_CHANNEL_LEFT = 0,
    O_CHANNEL_RIGHT = 4,
    O_CHANNEL_CENTER = 8,

    O_CHANNEL_MASK = 12,

    O_POSITION_LEFT = 0,
    O_POSITION_RIGHT = 16,
    O_POSITION_CENTER = 32,

    O_POSITION_MASK = 48,
};

enum new_effect {
    N_POSITION_LEFT = 1,
    N_POSITION_RIGHT = 2,
    N_POSITION_CENTER = 4,
    N_WAVEFORM = 8,
    N_SPECTRUM = 16,
};

typedef struct {
    int old_effect;
    int new_effect;
    int num_colours;
    int colour[16];
    int size;
    int colour_pos;
    int source;
    int render_position;
} RingContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    RingContext *ring = ctx->priv;
    int pos = 0;
    R32(ring->old_effect);
    R32(ring->num_colours);
    int temp = read_colours(buf + pos, buf_len - pos, ring->num_colours, 16, ring->colour);
    if (temp < 0)
        return -1;
    pos += temp;
    R32(ring->size);
    R32(ring->source);

    /* Translate the old channel bitfield values. */
    switch (ring->old_effect & O_CHANNEL_MASK) {
        case O_CHANNEL_LEFT:
            ctx->which_channel = VIS_LEFT;
            break;
        case O_CHANNEL_RIGHT:
            ctx->which_channel = VIS_RIGHT;
            break;
        case O_CHANNEL_CENTER:
            ctx->which_channel = VIS_CENTER;
            break;
        default:
            ERROR("unknown effect\n");
            return -1;
    }

    /* Translate the old position bitfield values. */
    switch (ring->old_effect & O_POSITION_MASK) {
        case O_POSITION_LEFT:
            ring->new_effect |= N_POSITION_LEFT;
            break;
        case O_POSITION_RIGHT:
            ring->new_effect |= N_POSITION_RIGHT;
            break;
        case O_POSITION_CENTER:
            ring->new_effect |= N_POSITION_CENTER;
            break;
        default:
            ERROR("unknown effect\n");
            return -1;
    }

    /* Translate the old source value. */
    if (ring->source) {
        ctx->which_vis_type = VIS_SPECTRUM;
        ring->new_effect |= N_SPECTRUM;
    } else {
        ctx->which_vis_type = VIS_WAVEFORM;
        ring->new_effect |= N_WAVEFORM;
    }

    return 0;
}

static void print_config(ComponentContext *ctx, unused int indent)
{
    RingContext *ring = ctx->priv;
    fprintf(stderr, "    size = %d\n", ring->size);
}

static int render(ComponentContext *ctx, uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    RingContext *ring = ctx->priv;
    int colour = interpolate_colours_64(ring->num_colours, ring->colour, &ring->colour_pos);

    float s = ring->size / 32.0;
    float is = MIN(h * s, w * s);
    int c_y = h / 2;
    int c_x;

    if (ring->new_effect & N_POSITION_LEFT)
        c_x = w / 4;
    else if (ring->new_effect & N_POSITION_RIGHT)
        c_x = (3 * w) / 4;
    else
        c_x = w / 2;

    float a = 0.0;
    float sca;

    if (ring->new_effect & N_WAVEFORM)
        sca = 0.1 + (vis_data[0] / 255.0) * 0.9;
    else
        sca = 0.1 + ((vis_data[0] + vis_data[1]) / 510.0) * 0.9;

    int lx = c_x + cosf(a) * is * sca;
    int ly = c_y + sinf(a) * is * sca;

    for (int q = 1; q <= 80; q++) {
        a -= M_PI * 2.0 / 80.0;

        if (ring->new_effect & N_WAVEFORM)
            sca = 0.1 + (vis_data[q > 40 ? 80 - q : q] / 255.0) * 0.9;
        else
            sca = 0.1 + ((vis_data[q > 40 ? (80 - q) * 2 : q * 2]
                        + vis_data[q > 40 ? (80 - q) * 2 + 1 : q * 2 + 1]) / 510.0) * 0.9;

        int tx = c_x + cosf(a) * is * sca;
        int ty = c_y + sinf(a) * is * sca;

        line(frame, tx, ty, lx, ly, w, h, colour, ctx->blend_mode);

        lx = tx;
        ly = ty;
    }

    return 0;
}

Component r_ring = {
    .name = "Render / Ring",
    .code = 14,
    .priv_size = sizeof(RingContext),
    .load_config = load_config,
    .print_config = print_config,
    .render = render,
};
