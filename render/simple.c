/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "linedraw.h"
#include "pixel.h"

/* Not very ordered but copies the values from the original. */
enum old_render_effect {
    O_SPECTRUM_LINES = 1,
    O_SPECTRUM_SOLID = 0,
    O_SPECTRUM_DOTS = 64,
    O_WAVEFORM_LINES = 2,
    O_WAVEFORM_SOLID = 3,
    O_WAVEFORM_DOTS = 66,
};

enum new_render_effect {
    N_SPECTRUM = 1,
    N_WAVEFORM = 2,
    N_LINES = 4,
    N_SOLID = 8,
    N_DOTS = 16,

    N_SPECTRUM_LINES = N_SPECTRUM|N_LINES,
    N_SPECTRUM_SOLID = N_SPECTRUM|N_SOLID,
    N_SPECTRUM_DOTS = N_SPECTRUM|N_DOTS,
    N_WAVEFORM_LINES = N_WAVEFORM|N_LINES,
    N_WAVEFORM_SOLID = N_WAVEFORM|N_SOLID,
    N_WAVEFORM_DOTS = N_WAVEFORM|N_DOTS,
};

typedef struct {
    int render_effect;
    int render_position;
    int num_colours;
    int colour[16];
    int colour_pos;
} SimpleContext;

#define util_get_audio_channel(dst, src) { \
    if ((src) >= 3) { \
        ERROR("invalid audio channel (%d)\n", (src)); \
        return -1; \
    } \
    (dst) = (src); \
}

#define util_get_position_y(dst, src) { \
    if ((src) >= 3) { \
        ERROR("invalid Y position (%d)\n", (src)); \
        return -1; \
    } \
    (dst) = (src); \
}

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    SimpleContext *sctx = ctx->priv;
    int effect, num_colours, temp;
    int pos = 0;

    R32(effect);

    switch (effect & (64|2|1)) {
        case O_SPECTRUM_LINES:
            sctx->render_effect = N_SPECTRUM_LINES;
            break;
        case O_SPECTRUM_SOLID:
            sctx->render_effect = N_SPECTRUM_SOLID;
            break;
        case O_SPECTRUM_DOTS:
            sctx->render_effect = N_SPECTRUM_DOTS;
            break;
        case O_WAVEFORM_LINES:
            sctx->render_effect = N_WAVEFORM_LINES;
            break;
        case O_WAVEFORM_SOLID:
            sctx->render_effect = N_WAVEFORM_SOLID;
            break;
        case O_WAVEFORM_DOTS:
            sctx->render_effect = N_WAVEFORM_DOTS;
            break;
        default:
            ERROR("unknown render effect\n");
            return -1;
    }

    if (sctx->render_effect & N_SPECTRUM)
        ctx->which_vis_type = VIS_SPECTRUM;
    else
        ctx->which_vis_type = VIS_WAVEFORM;

    if (((effect >> 2) & 3) == 0)
        ctx->which_channel = VIS_LEFT;
    else if (((effect >> 2) & 3) == 1)
        ctx->which_channel = VIS_RIGHT;
    else
        ctx->which_channel = VIS_CENTER;

    util_get_position_y(sctx->render_position, (effect >> 4) & 3);

    R32(num_colours);
    temp = read_colours(buf + pos, buf_len - pos, num_colours, 16, sctx->colour);
    if (temp < 0)
        return -1;
    pos += temp;
    sctx->num_colours = num_colours;

    return 0;
}

static void print_config(ComponentContext *ctx, unused int indent)
{
    SimpleContext *sctx = ctx->priv;
    const char *audio_source = "";
    const char *render_type = "";

    if (sctx->render_effect & N_SPECTRUM)
        audio_source = "spectrum";
    else if (sctx->render_effect & N_WAVEFORM)
        audio_source = "waveform";

    if (sctx->render_effect & N_LINES)
        render_type = "lines";
    else if (sctx->render_effect & N_SOLID)
        render_type = "solid";
    else if (sctx->render_effect & N_DOTS)
        render_type = "dots";

#define PRINT1(var) fprintf(stderr, "    " #var " = %s\n", var)
#define PRINT2(text, var) fprintf(stderr, "    " text " = %s\n", var)

    PRINT1(audio_source);
    PRINT1(render_type);
    PRINT2("render_position", get_position_y_desc(sctx->render_position));

    fprintf(stderr, "    num_colours = %d\n", sctx->num_colours);
    for (int i = 0; i < sctx->num_colours; i++)
        fprintf(stderr, "        colour[%d] = #%06x\n", i, sctx->colour[i]);
}

static int render(ComponentContext *ctx, uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, unused int is_beat)
{
    SimpleContext *sctx = ctx->priv;
    int y_baseline, colour;

    float y_scale = h / 512.0;
    float x_scale = 575.0 / (w - 1); /* Why did this not use all 576 samples? */

    /* All this could be moved into load_config.  Why bother doing this
     * everytime render is called?  Granted it probably isn't very slow. */
    colour = interpolate_colours_64(sctx->num_colours, sctx->colour, &sctx->colour_pos);

    int y_pos = sctx->render_position;

    if (sctx->render_effect & N_WAVEFORM) {
        if      (y_pos == 0) y_baseline = 0;
        else if (y_pos == 1) y_baseline = h/2;
        else                 y_baseline = h/4;
    } else {
        if      (y_pos == 0) y_baseline = h/2;
        else if (y_pos == 1) y_baseline = h/2;
        else                 y_baseline = (3*h)/4;
    }

    switch (sctx->render_effect) {
        case N_WAVEFORM_DOTS: {
            for (int x = 0; x < w; x++) {
                float r = x * x_scale;
                float s1 = r - (int)r;
                float yr = vis_data[(int)r] * (1.0 - s1)
                         + vis_data[(int)r + 1] * s1;
                int y = y_baseline + yr * y_scale;
                blend_pixel(frame + y * w + x, colour, ctx->blend_mode);
            }
            break; }
        case N_SPECTRUM_DOTS: {
            x_scale = x_scale / 2.0;
            if (y_pos != 1)
                y_scale = -y_scale;

            /* Old adj variable seemed to exist to allow the (possibly bugged)
             * line drawing function to draw a 1-height line. */
            for (int x = 0; x < w; x++) {
                float r = x * x_scale;
                float s1 = r - (int)r;
                float yr = vis_data[(int)r] * (1.0 - s1)
                         + vis_data[(int)r + 1] * s1;
                int y = y_baseline + yr * y_scale; /* Why was the -1 one here? */
                blend_pixel(frame + y * w + x, colour, ctx->blend_mode);
            }
            break; }
        case N_SPECTRUM_SOLID: {
            x_scale = x_scale / 2.0;
            if (y_pos != 1)
                y_scale = -y_scale;

            for (int x = 0; x < w; x++) {
                float r = x * x_scale;
                float s1 = r - (int)r;
                float yr = vis_data[(int)r] * (1.0 - s1)
                         + vis_data[(int)r + 1] * s1;
                int y = y_baseline + yr * y_scale; /* Again, a strange -1. */
                line(frame, x, y_baseline, x, y, w, h, colour, ctx->blend_mode);
            }
            break; }
        case N_SPECTRUM_LINES: {
            struct point { int x; int y; } p0, p1;

            x_scale = 2.0 / x_scale;
            p0 = (struct point){ 0, y_baseline + vis_data[0] * y_scale };

            if (y_pos != 1)
                y_scale = -y_scale;

            /* Why not all 576 samples? */
            for (int x = 1; x < 288; x++) {
                p1 = (struct point) { x * x_scale, y_baseline + vis_data[x] * y_scale };
                line(frame, p0.x, p0.y, p1.x, p1.y, w, h, colour, ctx->blend_mode);
                p0 = p1;
            }
            break; }
        case N_WAVEFORM_LINES: {
            struct point { int x; int y; } p0, p1;

            x_scale = 1.0 / x_scale; /* x_scale = 288/w, xs = w/288 */
            p0 = (struct point){ 0, y_baseline + vis_data[0] * y_scale };

            /* Again, why not all 576 samples? */
            for (int x = 1; x < 288; x++) {
                p1 = (struct point){ x * x_scale, y_baseline + vis_data[x] * y_scale };
                line(frame, p0.x, p0.y, p1.x, p1.y, w, h, colour, ctx->blend_mode);
                p0 = p1;
            }
            break; }
        case N_WAVEFORM_SOLID: {
            int y_center = y_baseline + h/4 - 1;
            for (int x = 0; x < w; x++) {
                float r = x * x_scale;
                float s1 = r - (int)r;
                float yr = vis_data[(int)r] * (1.0 - s1)
                         + vis_data[(int)r + 1] * s1;
                int y = y_baseline + yr * y_scale;
                line(frame, x, y_center, x, y, w, h, colour, ctx->blend_mode);
            }
            break; }
    }

    return 0;
}

Component r_simple = {
    .name = "Render / Simple",
    .code = 0,
    .priv_size = sizeof(SimpleContext),
    .load_config = load_config,
    .print_config = print_config,
    .render = render,
};
