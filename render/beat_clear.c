/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int colour;
    int blend;
    int num_beats;
    int beat_count;
} BeatClearContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    BeatClearContext *bctx = ctx->priv;
    int pos = 0;
    R32(bctx->colour);
    R32(bctx->blend);
    R32(bctx->num_beats);
    return 0;
}

static void print_config(ComponentContext *ctx, unused int indent)
{
    BeatClearContext *bctx = ctx->priv;
    fprintf(stderr, "    colour = #%06x\n", bctx->colour);
    fprintf(stderr, "    num_beats = %d\n", bctx->num_beats);
    fprintf(stderr, "    blend = %d\n", bctx->blend);
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    BeatClearContext *bctx = ctx->priv;

    if (is_beat) {
        bctx->beat_count++;
        if (bctx->num_beats && bctx->beat_count >= bctx->num_beats) {
            bctx->beat_count = 0;
            int i = w * h;
            int c = bctx->colour;

            if (!bctx->blend)
                while (i--)
                    *frame++ = c;
            else
                while (i--) {
                    frame[0] = blend_pixel_avg(*frame, c);
                    frame++;
                }
        }
    }

    return 0;
}

Component r_beatclear = {
    .name = "Render / On Beat Clear",
    .code = 5,
    .priv_size = sizeof(BeatClearContext),
    .load_config = load_config,
    .print_config = print_config,
    .render = render,
};
