Advanced Visualization Studio
=============================

This is trying to be a port of Nullsoft's Advanced Visualization Studio.  It
uses that code as a base to recreate all the features while at the same time
rewriting it in, what I hope is, portable C99.

1 - Documentation
-----------------

What few docs there are, are in the doc/ directory.  There are also comments
in source files.  I will try to gather related things together.

2 - Licensing
-------------

The original code was released under a Modified or 3-clause BSD license.  Look
at COPYING.Nullsoft for the full details.  This project, however, is released
under GNU General Public License, version 3.  This can be found in full in
COPYING.GPLv3.

I am aware that this license may not be the most useful to people who want to
use this library.  As this project comes closer to being "finished" and there is
interest in using it, I will reconsider a change in license.  Due to that, if
you wish to contribute something I will ask that you agree to license it under
some lax license, perhaps one of: ISC, BSD-2, BSD-3, LGPL.

If you want a copy of The original AVS code, ask and I will give you a copy.

3 - Building
------------

Just run make and you will get a library to link into a program.  You can change
the makefile which has various compile settings at the top.  Since I work on an
x86 computer I unconditionally use various x86-only settings.

avs.h is the public header to include in your code.  Then link with libavs.a.
Lua is used in this project so you will also need to link to a Lua library.  I
have tested that it compiles and links with: Lua 5.1, Lua 5.2, and LuaJIT 2.0.3.
So any of those libraries should work fine.

