Original bugs
=============

Here I will list what I think are bugs in the original code.  To the authors:
sorry but, I think these problems exist and I can show them in some cases.

r_fadeout.cpp
-------------

Lines 82-84:

The colour lookup table is built, the red values are put in fadtab[0] and blue
values in fadtab[2].  When this table is used to render the frame the value in
fadtab[0], red, is written into the first byte.  In many places blue is assumed
to be in the least-significant byte.

This creates a problem on little-endian platforms, such as x86.  An array of
integers will have the least-significant byte first.  When this table is used
the wrong colours are rendered.

The MMX code must use the table correctly because I cannot see this problem
using the compiled AVS with Winamp.  CORRECTION: the MMX code does not use the
table at all.

r_sscope.cpp
------------

Line 286:

In the loop over the number of points to draw, a factor of 576.0 is used to
scale a variable which is then used to access an element in the visdata array.
When the loop reaches the last point being drawn the value at index [576] is
read.  This is 1 beyond the end of the array.

I can see this quite clearly if I use the "Spiral" preset of the superscope.
There is a little tick up at the end of the spiral.

Line 376:

The "Swirlie Dots" preset appears to have a typo.  `x = di;sin(u*di) * .4`  This
line in the points text appears to have a misplaced semi-colon.  Possibly it
should be a multiplication, or the `di` preceding it should be deleted.  With it
deleted, the points form a nice circle.  With a multiplication, they form a
spiral-like shape.

r_chanshift.cpp / resource.h / res.rc
-------------------------------------

For years I have wondered why the RGB mode (i.e. no change) of the Channel Shift
component appeared never to work.  I now think that this might be because of a
mismatch in the value by which is represented.

In r_chanshift.cpp it is represented by the preprocessor define of IDC_RGB,
which is defined as 1183 in resource.h.  In res.rc (and in the compiled version
2.82 vis_avs.dll according to Resource Hacker) the RGB mode is represented by
1023.

When I changed the value in the dialog using Resource Hacker it now works.

The g_DlgProc function in r_chanshift.cpp asks whether a button is checked by
iterating of a list of IDs.  The code here uses 1183, again, to represent the
RGB mode.  So when asked whether a button with that ID (which I presume does not
exist) is checked, Windows must be saying no.  When the value is corrected,
Windows obviously says yes.

