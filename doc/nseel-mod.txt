__declspec ( naked ) void nseel_asm_mod(void)
{
    __asm
    {
        fld qword ptr [ebx]

        fld qword ptr [eax]            // load denom
        fsub dword ptr [g_cmpaddtab+4] // subtract 1.0
        fabs                           // abs
        fadd qword ptr [eax]           // add denom
        fadd dword ptr [g_cmpaddtab+4] // add 1.0
        fmul dword ptr [g_half]        // multiply by 0.5
                                       // denom = (abs(denom - 1.0) + denom + 1.0) * 0.5
                                       // This makes it: denom = MAX(denom, 1.0)
        fistp dword ptr [esi]
        fistp dword ptr [esi+4]
        mov eax, [esi+4]
        xor edx, edx
        div dword ptr [esi]
        mov [esi], edx
        fild dword ptr [esi]
        mov eax, esi
        fstp qword ptr [esi]
        add esi, 8
    }
}
