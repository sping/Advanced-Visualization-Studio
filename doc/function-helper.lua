__avs_close_fact = 0.00001

abs = math.abs
acos = math.acos
asin = math.asin
atan = math.atan
atan2 = math.atan2

function band(value,value2)
    if abs(value) > __avs_close_fact and abs(value2) > __avs_close_fact then
        return 1.0
    else
        return 0.0
    end
end

function bnot(value)
    if abs(value) > __avs_close_fact then
        return 0.0
    else
        return 1.0
    end
end

function bor(value,value2)
    if abs(value) > __avs_close_fact or abs(value2) > __avs_close_fact then
        return 1.0
    else
        return 0.0
    end
end

ceil = math.ceil
cos = math.cos
exp = math.exp
floor = math.floor

function invsqrt(value)
    return pow(value, -0.5)
end

log = math.log

function log10(value)
    return log(value, 10)
end

max = math.max
min = math.min
pow = math.pow

function rand(value)
    -- The original text explaining this function states that it returns an
    -- integer in the range 0..value.  This might want to follow those limits.
    return math.random() * value
end

function sigmoid(value,value2)
    local t = (1 + exp(-1 * value * value2))
    if t > __avs_close_fact then
        return 1.0 / t
    else
        return 0.0
    end
end

-- Yes this should return zero or negative zero.
function sign(value)
    if value > 0.0 then
        return 1.0
    elseif value < 0.0 then
        return -1.0
    elseif value == -0.0 then
        return -0.0
    else
        return 0.0
    end
end

sin = math.sin

function sqr(value)
    return value * value
end

sqrt = math.sqrt
tan = math.tan

function equal(value,value2)
    if abs(value-value2) > __avs_close_fact then
        return 0.0
    else
        return 1.0
    end
end

function above(value,value2)
    if value > value2 then
        return 1.0
    else
        return 0.0
    end
end

function below(value,value2)
    if value < value2 then
        return 1.0
    else
        return 0.0
    end
end

function __avs_if(value1, value2, value3)
    if abs(value1) > __avs_close_fact then
        return value2
    else
        return value3
    end
end
