/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AVS_HEADER_COMMON
#define AVS_HEADER_COMMON 1

/* Colour helpers */

#define I3_TO_I(r,g,b) ((((int)(r)) << 16) | (((int)(g)) << 8) | ((int)(b)))
#define CR_TO_I(c) (((c) >> 16) & 255)
#define CG_TO_I(c) (((c) >>  8) & 255)
#define CB_TO_I(c)  ((c)        & 255)

int read_colours(const uint8_t *buf, int buf_len, int num_colours, int max_colours, int *colours);
int interpolate_colours_64(int num_colours, int *colour, int *colour_pos);

/* Math helpers */

#ifndef M_PI
#define M_PI 3.1415926535897932384626433832795
#endif

#ifndef M_EULER
#define M_EULER 2.7182818284590452353602874713527
#endif

#ifndef M_PHI
#define M_PHI 1.6180339887498948482045868343656
#endif

#define ABS(x) \
    (((x) < 0) ? -(x) : (x))

#define CLIP(a,b,c) \
    (MIN(MAX((a), (b)), (c)))

#define MAX(a,b) \
    ((a) > (b) ? (a) : (b))

#define MAX3(a,b,c) \
    (MAX(MAX((a), (b)), (c)))

#define MIN(a,b) \
    ((a) < (b) ? (a) : (b))

#define MIN3(a,b,c) \
    (MIN(MIN((a), (b)), (c)))

/* Memory helpers */

#define lengthof(a) ((int)(sizeof(a) / sizeof((a)[0])))

#define FREE(a) if (a) free(a)

/* Print helpers */

enum avs_log_level {
    AVS_LOG_INFO,
    AVS_LOG_ERROR,
    AVS_LOG_WARNING,
    AVS_LOG_VERBOSE,
    AVS_LOG_DEBUG,
};

void avs_log(int level, const char *fmt, ...);

#define ERROR(message, ...) \
    avs_log(AVS_LOG_ERROR, "(%s:%d) [ERROR] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define WARNING(message, ...) \
    avs_log(AVS_LOG_WARNING, "(%s:%d) [WARNING] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define INFO(message, ...) \
    avs_log(AVS_LOG_INFO, "(%s:%d) [INFO] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define VERBOSE(message, ...) \
    avs_log(AVS_LOG_VERBOSE, "(%s:%d) [VERBOSE] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define DEBUG(message, ...) \
    avs_log(AVS_LOG_DEBUG, "(%s:%d) [DEBUG] " message, __FILE__, __LINE__, ##__VA_ARGS__)

#define CONT(message, ...) \
    avs_log(AVS_LOG_INFO, message, ##__VA_ARGS__)

const char *get_audio_channel_desc(int value);
const char *get_position_y_desc(int value);

/* Reading helpers */

#define read_s32le(buf) \
    (((int)((const uint8_t *)(buf))[3] << 24) | \
          (((const uint8_t *)(buf))[2] << 16) | \
          (((const uint8_t *)(buf))[1] <<  8) | \
           ((const uint8_t *)(buf))[0])

#define R32(var) { \
    if (pos > buf_len - 4) { \
        ERROR("unable to parse file correctly (too short)\n"); \
        return -1; \
    } \
    var = read_s32le(buf + pos); \
    pos += 4; \
}

typedef union {
    int i;
    float f;
} fi_union;

#define R32F(var) \
    if (pos < buf_len - 4) { \
        fi_union temp; \
        temp.i = read_s32le(buf + pos); \
        var = temp.f; \
        pos += 4; \
    }

#endif
