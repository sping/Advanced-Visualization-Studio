/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "avs.h"
#include "blend.h"
#include "bpm.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

#if ARCH_X86
void init_x86(int cpu_flags);
#endif

extern const Component effect_list;

static const char sig_0_1[] = "Nullsoft AVS Preset 0.1\x1a";
static const char sig_0_2[] = "Nullsoft AVS Preset 0.2\x1a";
static const int  sig_len   = sizeof(sig_0_2) - 1; /* Don't count the null char. */

static int read_file(const char *filename, uint8_t **out_buffer, int *out_size)
{
    int size, count;
    uint8_t *buffer = NULL;
    FILE *fh = NULL;

    fh = fopen((const char *)filename, "rb");
    if (!fh)
        goto error;

    if (fseek(fh, 0, SEEK_END) < 0)
        goto error;

    size = ftell(fh);
    if (size < 0 || size > 1048576) /* vis_avs uses a constant 1MiB buffer, */
        goto error;                 /* this makes for a useful sanity check */
                                    /* on the size. */

    if (fseek(fh, 0, SEEK_SET) < 0)
        goto error;

    buffer = malloc(size);
    if (!buffer)
        goto error;

    count = fread(buffer, 1, size, fh);
    if (count != size)
        goto error;

    *out_buffer = buffer;
    *out_size = size;

    fclose(fh);
    return 0;

error:
    *out_buffer = NULL;
    *out_size = 0;

    if (buffer)
        free(buffer);
    if (fh)
        fclose(fh);

    return -1;
}

static int load_preset(AVSContext *actx, const char *filename)
{
    int preset_size, ret;
    uint8_t *preset_buf = NULL;


    if (read_file(filename, &preset_buf, &preset_size)) {
        ERROR("Unable to read file: %s\n", filename);
        ret = -1;
        goto error;
    }

    if (preset_size < sig_len) {
        ERROR("preset file is too short\n");
        ret = -1;
        goto error;
    }

    /* Check that the signature string is present and correct.  There may be
     * some differences between the 0.1 and 0.2 versions so if a version 0.1
     * header is found, print a warning and ask for a report. */
    if (!memcmp(preset_buf, sig_0_1, sig_len)) {
        WARNING("AVS preset version 0.1 header detected.  Continuing but "
                "please report this to the Advanced Visualization Studio "
                "project\n");
    } else if (memcmp(preset_buf, sig_0_2, sig_len)) {
            ERROR("invalid preset header\n");
            ret = -1;
            goto error;
    }

    actx->root_list.priv = calloc(1, effect_list.priv_size);
    if (!actx->root_list.priv) {
        ret = -1;
        goto error;
    }

    actx->root_list.component = &effect_list;
    actx->root_list.blend_mode = blend_mode_default();
    actx->root_list.actx = actx;

    ret = effect_list.load_config(&actx->root_list, preset_buf + sig_len, preset_size - sig_len);

error:
    if (preset_buf)
        free(preset_buf);

    return ret;
}

void avs_print_preset(AVSContext *actx)
{
    if (!actx)
        return;

    effect_list.print_config(&actx->root_list, 0);

    return;
}

int avs_init(AVSContext **actx_out, const char *filename, int cpu_flags)
{
    AVSContext *actx = calloc(1, sizeof(AVSContext));
    if (!actx) {
        ERROR("no memory\n");
        return -1;
    }

    actx->beat_context = init_bpm(BEAT_ADVANCED);
    if (!actx->beat_context)
        return -1;

    register_components();
    if (load_preset(actx, filename)) {
        return -1;
    }

    /* Initialize things. */
    init_blend_table();

#if ARCH_X86
    init_x86(cpu_flags);
#endif

    *actx_out = actx;
    return 0;
}

int avs_render_frame(AVSContext *actx, AVSDataContext *avsdc, uint32_t time)
{
    /* TODO: fill in missing data. */
    /* TODO: new frames for components that need it. */

    int w = avsdc->width, h = avsdc->height;
    int beat = 0, beat2, lt[2] = {0, 0};

    /* Beat detection. */
    for (int i = 0; i < 576; i++) {
        lt[0] += abs(avsdc->waveform.left[i] - 128);
        lt[1] += abs(avsdc->waveform.right[i] - 128);
    }
    lt[0] = MAX(lt[0], lt[1]);
    actx->beat_peak1 = (actx->beat_peak1 * 125 + actx->beat_peak2 * 3) / 128;
    actx->beat_count++;
    if (lt[0] >= (actx->beat_peak1 * 34) / 32 && lt[0] > 576 * 16) {
        if (actx->beat_count > 0) {
            actx->beat_count = 0;
            beat = 1;
        }
        actx->beat_peak1 = (lt[0] + actx->beat_peak1_peak) / 2;
        actx->beat_peak1_peak = lt[0];
    } else if (lt[0] > actx->beat_peak2)
        actx->beat_peak2 = lt[0];
    else
        actx->beat_peak2 = (actx->beat_peak2 * 14) / 16;
    beat2 = refine_beat(actx->beat_context, beat, (time) ? time : 1);

    /* Frame management. */
    if (!actx->frame[0] && !actx->frame[1]) {
            actx->frame[0] = calloc(w * h, sizeof(int));
            actx->frame[1] = calloc(w * h, sizeof(int));
            if (!actx->frame[0] || !actx->frame[1])
                return -1;
    }

    if (actx->clear_every_frame) {
        memset(actx->frame[0], 0, w * h * sizeof(int));
        memset(actx->frame[1], 0, w * h * sizeof(int));
    }

    actx->avsdc = *avsdc;

    int ret = effect_list.render(&actx->root_list, NULL, actx->frame[0], actx->frame[1], w, h, beat2);
    if (ret < 0)
        return -1;

    if (ret == 1) {
        void *temp = actx->frame[0];
        actx->frame[0] = actx->frame[1];
        actx->frame[1] = temp;
    }

    return 0;
}

void *avs_get_frame(AVSContext *actx)
{
    return actx->frame[0];
}

void avs_uninit(AVSContext *actx)
{
    if (!actx)
        return;

    FREE(actx->beat_context);

    if (actx->root_list.priv) {
        effect_list.uninit(&actx->root_list);
        free(actx->root_list.priv);
    }

    for (int i = 0; i < lengthof(actx->global_buffer); i++)
        FREE(actx->global_buffer[i]);

    FREE(actx->frame[0]);
    FREE(actx->frame[1]);

    free(actx);
}
