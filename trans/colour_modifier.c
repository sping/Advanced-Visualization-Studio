/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "expressions.h"

typedef struct {
    const char *expr_init;
    const char *expr_frame;
    const char *expr_beat;
    const char *expr_level;
    void *expr_context;
    int recompute;
    int tab_valid;
    int inited;
    uint8_t tab[256*3];
} ColourModifierContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    ColourModifierContext *mod = ctx->priv;
    int pos = 0;

    void *expr_ctx = expr_init(ctx->actx);
    if (!expr_ctx)
        return -1;

    if (*buf == 1) {
        int ret = 0;
        pos++;

        ret = expr_load_rstring(expr_ctx, &mod->expr_level, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &mod->expr_frame, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &mod->expr_beat, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;

        ret = expr_load_rstring(expr_ctx, &mod->expr_init, buf + pos, buf_len - pos);
        if (ret < 0)
            return -1;
        pos += ret;
    } else {
        /* read fixed len */
            ERROR("fixed-length expressions not yet implemented\n");
    }

    R32(mod->recompute);

    if (expr_compile(expr_ctx, "__avs_internal_init", mod->expr_init)
            || expr_compile(expr_ctx, "__avs_internal_frame", mod->expr_frame)
            || expr_compile(expr_ctx, "__avs_internal_beat", mod->expr_beat)
            || expr_compile(expr_ctx, "__avs_internal_level", mod->expr_level))
        return -1;

    mod->expr_context = expr_ctx;

    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    ColourModifierContext *mod = ctx->priv;

#define SET(a,b) expr_variable_set(mod->expr_context, (a), (b))
#define GET(a) expr_variable_get(mod->expr_context, (a))

    SET("b", is_beat ? 1.0 : 0.0);

    if (!mod->inited) {
        if(expr_execute(mod->expr_context, "__avs_internal_init", mod->expr_init))
            return -1;
        mod->inited = 1;
    }

    if (expr_execute(mod->expr_context, "__avs_internal_frame", mod->expr_frame))
        return -1;

    if (is_beat)
        if (expr_execute(mod->expr_context, "__avs_internal_beat", mod->expr_beat))
            return -1;

    if (mod->recompute || !mod->tab_valid) {
        for (int i = 0; i < 256; i++) {
            SET("red", i / 255.0);
            SET("green", i / 255.0);
            SET("blue", i / 255.0);

            if (expr_execute(mod->expr_context, "__avs_internal_level", mod->expr_level))
                return -1;

            int r = CLIP(GET("red")   * 255.0 + 0.5, 0, 255);
            int g = CLIP(GET("green") * 255.0 + 0.5, 0, 255);
            int b = CLIP(GET("blue")  * 255.0 + 0.5, 0, 255);

            mod->tab[i+512] = r;
            mod->tab[i+256] = g;
            mod->tab[i    ] = b;
        }

        mod->tab_valid = 1;
    }

    uint8_t *fb = (uint8_t *)frame;
    int pixels = w*h;

    while (pixels--) {
        fb[0] = mod->tab[fb[0]];
        fb[1] = mod->tab[fb[1] + 256];
        fb[2] = mod->tab[fb[2] + 512];
        fb += 4;
    }

    return 0;
}

static void uninit(ComponentContext *ctx)
{
    ColourModifierContext *mod = ctx->priv;

    FREE((void *)mod->expr_init);
    FREE((void *)mod->expr_frame);
    FREE((void *)mod->expr_beat);
    FREE((void *)mod->expr_level);

    expr_uninit(mod->expr_context);
}

Component t_colourmodifier = {
    .name = "Trans / Colour Modifier",
    .code = 45,
    .priv_size = sizeof(ColourModifierContext),
    .load_config = load_config,
    .render = render,
    .uninit = uninit,
};
