/*
 * Copyright (c) 2014 James Darnley <james.darnley@gmail.com>
 *
 * This File is part of Advanced Visualization Studio.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "attributes.h"
#include "avs.h"
#include "components.h"
#include "common.h"
#include "pixel.h"

typedef struct {
    int x, y;
    int enabled;
    int colour;
    int blend;
    int blend_avg;
    int on_beat;
    int x2, y2;
    int beat_dur;
    int cur_x, cur_y;
} InterleaveContext;

static int load_config(ComponentContext *ctx, const uint8_t *buf, int buf_len)
{
    InterleaveContext *ictx = ctx->priv;
    int pos = 0;
    R32(ictx->enabled);
    R32(ictx->x);
    R32(ictx->y);
    R32(ictx->colour);
    R32(ictx->blend);
    R32(ictx->blend_avg);
    R32(ictx->on_beat);
    R32(ictx->x2);
    R32(ictx->y2);
    R32(ictx->beat_dur);
    ictx->cur_x = ictx->x;
    ictx->cur_y = ictx->y;
    return 0;
}

static int render(ComponentContext *ctx, unused uint8_t vis_data[576], int *frame, unused int *frame_out, int w, int h, int is_beat)
{
    InterleaveContext *ictx = ctx->priv;
    if (!ictx->enabled)
        return 0;

    float sc1 = (ictx->beat_dur + 512.0 - 64.0) / 512.0;
    ictx->cur_x = ictx->cur_x * sc1 + ictx->x * (1.0 - sc1);
    ictx->cur_y = ictx->cur_y * sc1 + ictx->y * (1.0 - sc1);

    if (is_beat && ictx->on_beat) {
        ictx->cur_x = ictx->x2;
        ictx->cur_y = ictx->y2;
    }

    int tx = ictx->cur_x;
    int ty = ictx->cur_y;

    int xos = 0;
    int ystat = 0;
    int yp = 0;

    if (!ty)
        ystat = 1;

    if (tx > 0)
        xos = (w % tx) / 2;

    if (ty > 0)
        yp = (h % ty) / 2;

    /* This condition is flipped to save a level of indentation below. */
    if (tx < 0 || ty < 0)
        return 0;

    for (int j = 0; j < h; j++) {
        int xstat = 0;

        if (ty && ++yp >= ty) {
            ystat = !ystat;
            yp = 0;
        }

        if (!ystat) {
            if (ictx->blend) {
                for (int l = w; l; l--) {
                    *frame = blend_pixel_add(*frame, ictx->colour);
                    frame++;
                }
            } else if (ictx->blend_avg) {
                for (int l = w; l; l--) {
                    *frame = blend_pixel_avg(*frame, ictx->colour);
                    frame++;
                }
            } else {
                for (int l = w; l; l--)
                    *frame++ = ictx->colour;
            }
        } else if (tx) {
            if (ictx->blend) {
                int xo = xos;
                for (int l = w; l > 0;) {
                    int l2 = MIN(l, tx - xo);
                    l -= l2;
                    xo = 0;
                    if (xstat)
                        frame += l2;
                    else
                        while (l2--) {
                            *frame = blend_pixel_add(*frame, ictx->colour);
                            frame++;
                        }
                    xstat = !xstat;
                }
            } else if (ictx->blend_avg) {
                int xo = xos;
                for (int l = w; l > 0;) {
                    int l2 = MIN(l, tx - xo);
                    l -= l2;
                    xo = 0;
                    if (xstat)
                        frame += l2;
                    else
                        while (l2--) {
                            *frame = blend_pixel_avg(*frame, ictx->colour);
                            frame++;
                        }
                    xstat = !xstat;
                }
            } else {
                int xo = xos;
                for (int l = w; l > 0;) {
                    int l2 = MIN(l, tx - xo);
                    l -= l2;
                    xo = 0;
                    if (xstat)
                        frame += l2;
                    else
                        while (l2--)
                            *frame++ = ictx->colour;
                    xstat = !xstat;
                }
            }
        } else
            frame += w;
    }

    return 0;
}

Component t_interleave = {
    .name = "Trans / Interleave",
    .code = 23,
    .priv_size = sizeof(InterleaveContext),
    .load_config = load_config,
    .render = render,
};
